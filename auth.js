// we will this for authentication
// login
// retrieve user details

// Json Web Token
// methods
//  sign(data , secret, {})
// creates a token
// verify(token, secret, cb())
// check if the token is present
// decode(token, {}).payload
// interpret/decodes the token

const jwt = require('jsonwebtoken');
const User = require('./models/User');
const secret = 'CourseBookingApi';

// create a token
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin,
	};

	return jwt.sign(data, secret, {});
};

// verify token
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	// console.log(token);

		// token = bearer rewrewrfdsfewfdsvsdfewfwvcw
	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length); // para matangal ang bearer na word sa token
		
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return res.send({ auth: 'failed' });
				// para hindi na mag continue sa function sa userRoutes (auth.verify)
			} else {
				next();
			}
		});
	}
};

// decode token

module.exports.decode = (token) => {

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload
			}

		})
	}
}

// module.exports.decode = (token) => {

// 	if (typeof token !== 'undefined') {
// 		// console.log(token);
// 		token = token.slice(7, token.length);
// 		console.log(token);
// 		return jwt.verify(token, secret, (data, error) => {
// 			if(error){
// 				return null
// 			} else {
// 				return jwt.decode(token, {completed: true}).payload
// 			}
// 		});
// 	}
// }
