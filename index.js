const express = require( 'express' );
const mongoose = require( 'mongoose' );
const PORT = 3000;
const app = express();
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");
const cors = require('cors');


// middlewares
app.use( express.json() );
app.use( express.urlencoded( {extended:true} ) );
app.use( cors() );

// database mongoose connection
mongoose.connect("mongodb+srv://lawrence:admin@cluster0.psfw2.mongodb.net/course_booking?retryWrites=true&w=majority",
    {
        useNewUrlParser:true,
	    useUnifiedTopology:true
    }
).then( () => { 
	console.log( `Successfully Connected to Database!` );
} ).catch( ( error ) => { 
	console.log( error );
} );


// routes
app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);


app.listen(PORT, () => {
    console.log( `Server started on port`, PORT );
} );