const express = require('express');
const router = express.Router();
// auth
const auth = require('./../auth');

// controllers
const userController = require('./../controllers/userControllers');

// user registration
router.post('/register', (req, res) => {
    userController.register(req.body).then((result) => res.send(result));
});

// check if email exist
router.post('/checkEmail', (req, res) => {
    userController.checkEmailExist(req.body).then((result) => res.send(result));
});

// login
router.post('/login', (req, res) => {
    userController.login(req.body).then((result) => res.send(result));
});

// details
// auth.verify - naa sa auth.js yung verify para mag verify sa log in
router.get('/details', auth.verify, (req, res) => {
    // console.log(req.headers.authorization);
    const userData = auth.decode(req.headers.authorization);
    // console.log(userData);
    userController.getProfile(userData.id).then((result) => res.send(result));
});

// export router
module.exports = router;
