const express = require('express');
const router = express.Router();
let auth = require('./../auth');
const courseController = require('./../controllers/courseControllers');


// retrieve all courses
router.get('/active', (req, res) => {
    courseController.getAllActive().then(result => res.send(result));
});

router.get('/all', (req, res) => {
    courseController.getAllCourses().then(result => res.send(result));
});

router.post('/addCourse', auth.verify, (req, res) => {
    courseController.addCourse(req.body).then(result => res.send(result));
});
// make a route to get the single course
// make route to update the course
// make a route to archive the course
    // meaning update the status of the course from true to false
// make a route to unarchive the course
    // meaning update the status of the course frim false to true
// make a route to delete a course


module.exports = router;