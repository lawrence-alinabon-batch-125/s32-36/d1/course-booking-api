const User = require('./../models/User');
const bcrypt = require('bcrypt');
const auth = require('./../auth');
const { findOne } = require('./../models/User');
module.exports.checkEmailExist = (reqBody) => {
    // model.method
    return User.find({ email: reqBody.email }).then((result, error) => {
        if (result.length != 0) {
            return true;
        } else {
            return false;
        }
    });
};

module.exports.register = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10), // 10 represents how many times the password is encrypted
    });

    return newUser.save().then((result, error) => {
        if (error) {
            return error;
        } else {
            return true;
        }
    });
};

module.exports.login = (reqBody) => {
    // find email
    return User.findOne({ email: reqBody.email }).then((result) => {
        if (result == null) {
            return false;
        } else {
            // compare the password
            const isPasswordCorrect = bcrypt.compareSync(
                reqBody.password,
                result.password
            ); // return boolean value
            if (isPasswordCorrect === true) {
                return { access: auth.createAccessToken(result.toObject()) }; // toObject to convert to object
            } else {
                return false;
            }
        }
    });
};

module.exports.getProfile = (data) => {
    // console.log(data);
    // model.method
    return User.findById(data).then((result) => {
        result.password = "*******"
        return result
    })
};
