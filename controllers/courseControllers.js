const Course = require('./../models/Courses')

module.exports.getAllActive = () => {
    // Model.method - get all courses na active
    return Course.find({isActive : true}).then( result => { return result} )
}


// add course
module.exports.addCourse = (reqBody) => {
    let newCourse = new Course ({
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price
    })
    return newCourse.save().then((result, error) => {
        if (error) {
            return error;
        } else {
            return true;
        }
    })
}

module.exports.getAllCourses = () => {
    return Course.find().then( result => { return result})
}